class readystock extends kue{
    public readystock(String nama, double price, double jmlh) {
        super(nama, price);
        setJumlah(jmlh);
        
    }
    private double jmlh;
    public double hitungHarga(){
        return super.getPrice()*jmlh*2;
    }
    public void setJumlah(double jumlah){
        this.jmlh = jumlah;
    }
    public double getJumlah(){
        return jmlh;
    }
    @Override
    public double Berat() {
        return 0;
    }

    @Override
    public double Jumlah() {
        return jmlh;
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(price);
    }
}